import tkinter
from classes import Button

def info(state):
    # create window
    window = tkinter.Toplevel()
    window.title("Tietoa ohjelmasta")
    window.configure(bg=state.BG_COLOR) 
    window.resizable(False, False)

    # binds
    window.bind('<Escape>', lambda e : window.destroy())
    window.bind('<Return>', lambda e : window.destroy())

    #frames
    main_frame = tkinter.Frame(window,padx=15,pady=7,bg=state.BG_COLOR)
    header_frame = tkinter.Frame(main_frame,bg=state.BG_COLOR)
    
    # content
    icon = tkinter.Label(header_frame,image=state.puolukka,bg=state.BG_COLOR)
    header = tkinter.Label(header_frame,text="Puolukkakalenteri",
            font=(state.font,state.font_size-1),bg=state.BG_COLOR,fg=state.FG_PURPLE)
    label = tkinter.Label(main_frame,text="""Tekijä: Anna Soinio\n2021""",        bg=state.BG_COLOR,fg=state.FG_BLUE,
        font=(state.font,state.font_size-4))

    ok_button = Button(main_frame, "OK", state)
    ok_button.bind(window.destroy)
    
    # put everything in window
    main_frame.pack()
    header_frame.pack()

    icon.grid(column=0,row=0,padx=(0,5))
    header.grid(column=1,row=0)
    label.pack()
    ok_button.pack()

    window.mainloop()
