import tkinter
from tkinter import ttk
from datetime import date,datetime
from sys import stderr
from loadpaths import reminders_txt
from classes import AddReminderWin, Reminder

def write_reminder(state, add_reminder_win):
    # get text for reminder, return if empty
    reminderText = add_reminder_win.reminder_entry.get()

    if reminderText == "":
        return 0

    # add reminder to state.reminders list
    state.reminders.append(Reminder(date(*reversed(state.month_viewing),add_reminder_win.day+1),reminderText))

    # write reminder to file
    with open(reminders_txt(),"a",encoding="utf-8") as f:
        f.write(f"{add_reminder_win.day+1},{repr(state.month_viewing)},{reminderText}\n")

    # change day's bgcolor in grid
    if not state.isholiday(add_reminder_win.day+1,state.month_viewing.month):
        state.days[add_reminder_win.day]['background'] = state.REMINDER_BG
        state.days[add_reminder_win.day]['activebackground']=state.REMINDER_HOVER_BG

    # close the add reminder window
    add_reminder_win.close()
        
    # if main window expanded and adding a reminder for today,
    # add row to footer reminder list
    if state.expanded and date.today() == date(*reversed(state.month_viewing),add_reminder_win.day+1):
        state.footer_refresh()

def del_reminder(i: int, state, add_reminder_win=0):
    # destroy reminder
    if add_reminder_win != 0: # delete from AddReminderWin window
        text = add_reminder_win.rows[i].cget("text")
        day = add_reminder_win.day
        reminder_date = date(*reversed(state.month_viewing),day+1)

        add_reminder_win.x_buttons[i].destroy()
        add_reminder_win.rows[i].destroy()

        if len(add_reminder_win.reminder_list_frame.winfo_children())==0:
            add_reminder_win.reminder_list_frame.destroy()
            state.days[add_reminder_win.day]['background'] = state.LIGHT_PINK
            state.days[add_reminder_win.day]['activebackground']=state.LIGHTEST_PINK

        if date.today() == reminder_date and state.expanded:
            state.rows[i].destroy()
            state.x_buttons[i].destroy()

    else: # delete from main window
        day = date.today().day-1
        text = state.rows[i].cget('text')
        reminder_date = date.today()
        
        state.rows[i].destroy()
        state.x_buttons[i].destroy()
      
        if len(state.footer_rows_frame.winfo_children()) == 0:
            state.days[day]['background'] = state.LIGHT_PINK
            state.days[day]['activebackground']=state.LIGHTEST_PINK
            state.footer_rows_frame.destroy()

    # read lines from reminders file and write all but deleted one back into it
    with open(reminders_txt(),"r",encoding="utf-8") as fp:
        lines = fp.readlines()
    with open(reminders_txt(),"w",encoding="utf-8") as f:
        for line in lines:
            if line.strip("\n") != f"{day+1},{repr(state.month_viewing)},{text}":
                f.write(line)

    # remove from state.reminders list
    state.reminders.remove( Reminder( reminder_date, text) )
        
def add_reminder(day: int, state):
    # create instance of AddReminderWin
    add_reminder_win = AddReminderWin()

    add_reminder_win.day = day

    # create window and main frame
    add_reminder_win.window = tkinter.Toplevel()
    add_reminder_win.window.title("Lisää muistutus")
    add_reminder_win.window.configure(bg=state.BG_COLOR) 
    add_reminder_win.window.resizable(False, False)
    add_reminder_win.window.bind('<Escape>', lambda e : add_reminder_win.close())

    add_reminder_win.main_frame = tkinter.Frame(add_reminder_win.window,background=state.BG_COLOR,padx=3,pady=3)
    add_reminder_win.main_frame.grid(row=0,sticky="nsew")
    add_reminder_win.main_frame.grid_columnconfigure(0, weight=1)

    # create window header
    label = tkinter.Label(add_reminder_win.main_frame,
            text=f"Lisää muistutus: {state.weekdays.short[datetime(*reversed(state.month_viewing),day+1).weekday()]} {day+1}.{state.month_viewing.month}.{state.month_viewing.year}",
            background=state.BG_COLOR,foreground=state.FG_BLUE,font=(state.font,state.font_size-2))

    label.grid(row=0, columnspan = 5,sticky="w",padx=3)

    # create entry for new reminder
    add_reminder_win.reminder_entry = tkinter.Entry(add_reminder_win.main_frame,bg=state.LIGHTER_PINK,bd=1,fg=state.FG_PURPLE,font=("",state.font_size-4))
    add_reminder_win.reminder_entry.grid(row=1,column=0,padx=(0,4),sticky="we")
    
    add_reminder_win.reminder_entry.focus()

    # create add button
    add_reminder_button = tkinter.Button(add_reminder_win.main_frame, text="+",
            bg=state.BUTTON_BG,fg=state.BUTTON_FG,padx=7,pady=0,height=0,
            activebackground=state.HOVER_BUTTON_BG, activeforeground=state.HOVER_BUTTON_FG,
            font=("",state.font_size-1))
    add_reminder_button['command'] = lambda : write_reminder(state,add_reminder_win)
    add_reminder_button.grid(row=1,column=1,sticky="e")
    
    # bind enter key to entry (press enter to add reminder)
    add_reminder_win.reminder_entry.bind('<Return>',lambda e : write_reminder(state,add_reminder_win))

    # list day's reminders under entry
    add_reminder_win.reminder_list_frame = tkinter.Frame(add_reminder_win.main_frame,bg=state.LIGHT_PINK)
    add_reminder_win.reminder_list_frame.grid(row=2,sticky="nsew",columnspan="2")
    add_reminder_win.reminder_list_frame.grid_columnconfigure(0, weight=1)

    add_reminder_win.x_buttons = []
    
    for reminder in state.reminders:
        if reminder.date == date(*reversed(state.month_viewing),day+1):
            add_reminder_win.rows.append(tkinter.Label(add_reminder_win.reminder_list_frame,text=f"{reminder.text}",
                bg=state.LIGHT_PINK,fg=state.FG_PURPLE,justify="left",
                wraplength=220))
            # create delete buttons
            add_reminder_win.x_buttons.append(tkinter.Button(add_reminder_win.reminder_list_frame,
                text="⨯",padx=3,pady=0,
                bg=state.LIGHT_PINK,fg=state.FG_PURPLE))
    for holiday in state.holidays.list:
        if holiday.date == (day+1,state.month_viewing.month):
            add_reminder_win.rows.append(tkinter.Label(add_reminder_win.reminder_list_frame,text=f"{holiday.name}",
                bg=holiday.color,fg="#ffffff",justify="left", wraplength=220))

    # put reminders in grid
    for i,row in enumerate(add_reminder_win.rows):
        if row['bg'] == state.LIGHT_PINK:
            row.grid(row=i,column=0,sticky="nsew")
        else:
            row.grid(row=i,column=0,sticky="nsew",columnspan=2)

    # put delete buttons next to reminders
    for i,button in enumerate(add_reminder_win.x_buttons):
        button.grid(row=i,column=1,sticky="e")
        button['command'] = lambda num=i, s=state, arw=add_reminder_win : del_reminder(num,s,arw)


def close_todays_reminders(state,today_reminder_win):
    today_reminder_win.destroy()
    state.start()

def todays_reminders(state):
    todayStuff = 0
    for reminder in state.reminders:
        if reminder.date == date.today():
            todayStuff += 1
    # if there are no reminders, start main window
    if todayStuff == 0 and not state.isholiday(date.today().day):
        state.start()
        return
    
    # create window
    today_reminder_win = tkinter.Toplevel(state.window)
    today_reminder_win.resizable(False, False)

    main_frame = tkinter.Frame(today_reminder_win,bg=state.BG_COLOR,pady=5,padx=7)
    main_frame.grid(row=0,sticky="nsew")

    rows = []
    for reminder in state.reminders: # put todays reminders into labels in rows list
        if reminder.date == date.today():
            rows.append(tkinter.Label(main_frame,text=f"{reminder.text}",
                bg=state.LIGHT_PINK, fg=state.FG_PURPLE, wraplength=250))
    # if holiday, add it to rows
    holiday = state.isholiday(date.today().day)
    if holiday:
        rows.append(tkinter.Label(main_frame,text=f"{holiday.name}",
            bg=holiday.color,fg="#ffffff", wraplength=250))

    # config window and header
    today_reminder_win.title("Muistutukset")
    today_reminder_win.configure(bg=state.BG_COLOR)
    tod_win_title = tkinter.Label(main_frame,text="Tänään luvassa:",fg=state.FG_BLUE,bg=state.BG_COLOR,font=(state.font,14))
    tod_win_title.grid(row=0,padx=3,pady=2)

    separator = ttk.Separator(main_frame, orient='horizontal')
    separator.grid(row=1,column=0,sticky="we")

    for i,row in enumerate(rows): # put rows in grid
        row.grid(row=i+2,sticky="we")

    separator2 = ttk.Separator(main_frame, orient='horizontal')
    separator2.grid(row=99,column=0,sticky="we")

    # create ok button
    ok_button = tkinter.Button(main_frame,text="OK",bg=state.BUTTON_BG,
            fg=state.BUTTON_FG, activebackground=state.HOVER_BUTTON_BG,
            activeforeground=state.HOVER_BUTTON_FG,
            command = lambda : close_todays_reminders(state,today_reminder_win))
    ok_button.grid(row=100,pady=(5,0))
    ok_button.focus()

    # binds
    today_reminder_win.bind('<Escape>', lambda e : close_todays_reminders(state,today_reminder_win))
    today_reminder_win.bind('<Return>', lambda e : close_todays_reminders(state,today_reminder_win))

    today_reminder_win.mainloop()

def read_reminders():
    reminders = []
    try:
        with open(reminders_txt(),"r",encoding="utf-8") as f: 
            lines = f.readlines()
            try:
                lines.remove('\n')
            except ValueError:
                pass
            reminders = [line.rstrip().split(",", maxsplit=3) for line in lines]
            reminders = [Reminder(date(int(reminder[2]),int(reminder[1]),int(reminder[0])),reminder[3]) for reminder in reminders]
    except FileNotFoundError:
        pass
    return reminders
