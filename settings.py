import tkinter as tk
from classes import Button

def settings(state):
    # create window
    window = tk.Toplevel()
    window.title("Asetukset")
    window.configure(bg=state.BG_COLOR)
    window.resizable(False, False)

    # binds
    window.bind('<Escape>', lambda e : window.destroy())

    # button
    button = Button(window, "vaihda teemaa", state, margin_x=10, margin_y=10)
    button.bind(state.switch_theme)

    button.grid(row=0,column=0)

    window.mainloop()
