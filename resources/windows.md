# Asennus Windowsilla

- mene osoitteeseen https://python.org/downloads/windows/
- lataa Python:
    - oikea linkki löytyy kohdasta Stable Releases  
    - mahdollisesti "Download Windows Installer (64-bit)"
- suorita Python-installer ja asenna Python
- mene takaisin Puolukkakalenterin Gitlabin etusivulle lataa tästä napista Puolukkakalenteri

![download](resources/download.png)

- siirrä lataamasi tiedosto eli puolukka-kalenteri-master haluamaasi kansioon
- klikkaa hiiren oikealla napilla tiedostoa ja paina "Pura kaikki..."
- paina "Pura"
- mene kansioon puolukka-kalenteri-master ja sen sisällä olevaan samannimiseen kansioon
- tuplaklikkaa tiedostoa nimeltä "Kalenteri\_Windows"
