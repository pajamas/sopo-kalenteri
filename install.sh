#!/bin/bash

a=$(pwd)

puolukka=$a/resources/puolukka.png
echo -e "def puolukka_png():\n    return '$puolukka'" > loadpaths.py

reminders=$a/Reminders.txt
echo -e "\ndef reminders_txt():\n    return '$reminders'" >> loadpaths.py

icon=$a/resources/puolukkalogo.png
echo -e "\ndef icon_png():\n    return '$icon'" >> loadpaths.py

arrow=$a/resources/arrow.png
echo -e "\ndef arrow_png():\n    return '$arrow'" >> loadpaths.py


mkdir -p $HOME/.local/bin/
echo -e "#!/bin/bash\npython3 $a/kalenteri.py" > $HOME/.local/bin/puolukkakalenteri
#cp $a/kalenteri.py $HOME/.local/bin/puolukkakalenteri
chmod +x $HOME/.local/bin/puolukkakalenteri


desktop-file-install --dir=$HOME/.local/share/applications resources/puolukkakalenteri.desktop --set-key=Path --set-value=$HOME/.local/bin/ --set-icon=$a/resources/puolukkalogo.png
update-desktop-database ~/.local/share/applications
